﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SamplePro.DatabaseConnection;

namespace SamplePro.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ClassController : ControllerBase
    {
        public IActionResult GetClass()
        {
           
            using(var con = new AppDbContext())
            {
                var model = con.Tblclasses.ToList();
                return Ok(model); //sample 
            };
        }
    }
}
