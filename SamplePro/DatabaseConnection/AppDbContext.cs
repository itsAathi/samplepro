﻿using Microsoft.EntityFrameworkCore;

namespace SamplePro.DatabaseConnection
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
        {

        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=LAPTOP-RJ7FQBGI\\SQLEXPRESS;Database=EFGetStarted;Trusted_Connection=True;");
            }
        }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<TblClass> Tblclasses { get; set; }
    }

}


